//load express module to be able to use express properties and modules
let express = require('express');
const PORT = 3000;

let app = express();
	//app is now our server

//middlewares
	//setup for allowing the server to handle data from requests

app.use(express.json()); // allow your app to read json data
app.use(express.urlencoded({extended:true})); //allow your app to read data from the forms

//routes
	//2 parameters
app.get("/",(req, res) => res.send('Hello World'));

//mini activity
app.get("/hello", (req,res)=> res.send('Hello'));

//mini activity
app.post("/greetings", (req,res)=> {
	console.log(req.body)
	//console.log(`Hello there ${req.body.name}`);
	res.send(`Hello there ${req.body.name}`);
	//console.log(`Hello there`)


});



app.listen(PORT, ()=>{

	console.log(`Server running port ${PORT}`);

});
